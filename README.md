
---
title: an9elutils package
author: Angel Martinez-Perez
affiliation: Unit of Genomic of Complex Diseases
date: "Started: 2021-02-05"
description: compendium of common utils functions
last update: "2024-10-23"
---


<!-- social: start -->
[![](/home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4/an9elutils/figures/orcid.png)](https://orcid.org/0000-0002-5934-1454 'ORCID')[![](/home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4/an9elutils/figures/linkedin.png)](https://es.linkedin.com/in/angel-martinez-perez-28116a9 'linkedin')[![](/home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4/an9elutils/figures/scholar.png)](http://scholar.google.es/citations?user=IleVNpwAAAAJ 'google scholar')[![](/home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4/an9elutils/figures/rg.png)](https://www.researchgate.net/profile/Angel_Martinez-Perez/publications/ 'researchgate')[![](/home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4/an9elutils/figures/pubmed.png)](https://www.ncbi.nlm.nih.gov/myncbi/1liNzajtgcWkjb/bibliography/public/ 'pubmed')[![](/home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4/an9elutils/figures/twitter.png)](https://twitter.com/an9el_mp 'twitter')[<img src=/home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4/an9elutils/figures/IR.png width='60' height='24'/>](http://www.recercasantpau.cat/es/grupo/genomica-y-bioinformatica-de-enfermedades-de-base-genetica-compleja 'IIB Sant Pau')[<img src=/home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4/an9elutils/figures/wos.png width='24' height='24'/>](https://www.webofscience.com/wos/author/record/2108096 'WoS')
<!-- social: end -->



[[_TOC_]]


# Install



```r
devtools::install_gitlab("an9el/an9elutils")
```


# Multivariate analysis


## Predictive Power Score

Functions here:

* `pps.par` as a multicore wrapper to `visualize_pps` of
the [ppsr](https://github.com/paulvanderlaken/ppsr) package
* `pps.net` print with `visNetwork` the results of the previous function
* `pps.rm.interaction` to remove interactions in the pps results, within a group
* `pps.rm.feature` to remove one feature from the pps results

Example:



```r
df <- read_csv("https://raw.githubusercontent.com/8080labs/ppscore/master/examples/titanic.csv")
df <- df %>% 
    mutate( Survived = as.factor(Survived),
           Sex = as.factor(Sex),
           Class = as.factor(Pclass),
           Port = as.factor(Embarked)) %>% 
  mutate( across(where(is.character), as.factor) ) %>% 
  dplyr::select(
    Survived,
    Class,
    Sex,
    Age,
    TicketPrice = Fare,
    Port)
kk <- pps.par(df, parallel = TRUE)
groups <- data.frame(
    id = names(df),
    grs = c("response",
            rep("predictor",
                ncol(df)-1)))
plt <- pps.net(
    kk,
    dgroups = groups,
    filter = 0.06) %>%
    visNetwork::visLayout(randomSeed = 100)
plt %>% visNetwork::visSave("data/network.html")
```

# Other tools

* `normalizar` to normalize one vector
* `to_factor` given a data.frame this function code all variables as factor,
if have different values low different values.
* `drop_correlation` given a data.frame drop correlated features
* `drop_nearZero` given a data.frame drop near zero variance features
* `summarise_df` removed as `collapse::descr(df) %>% as.data.frame` is much better
* `plot_confusion` to plot confusion tables given the true and predicted values
* `plot_importance` barplot of a numeric vector
* `percent_value` calculate the percentage of each value in a numeric vector
* `factor_labels` given a factor return its labels values
* `recode_sex` change any type of sex notation to `0` as Male and `1` to Female
* `isnt_out` function generator of univariate detectors of outliers
* `is_mult_outlier` function to test for multivariate outliers, allow for
missing values
* `is_mult_outlier_imp` function to impute the multivariate outliers and
the missing values

## examples:

### Univariate outliers

Let's calculate the outliers of a vector with 3 methods. Remember FALSE is the outlier, and TRUE that is not a outlier:



```r
X <- c(NA, rnorm(44,0,1), NA, rnorm(4,0,30))
isnt_out_zscore_mean <- isnt_out(method = "zscore_mean")
isnt_out_funs <- tibble::lst(
         z = isnt_out(method = "zscore_mean", thres = 2),
         mad = isnt_out(method = "zscore_mad"),
         tukey = isnt_out(method = "tukey", k = 1.5)
         )
X_no_out <- X %>% data.frame %>%
    dplyr::transmute_if(is.numeric, isnt_out_funs)
X_no_out$value <- X
```
<!-- html table generated in R 4.2.2 by xtable 1.8-4 package -->
<!-- Thu Sep 14 10:32:19 2023 -->
<table border=1>
<caption align="bottom"> Not outliers </caption>
<tr> <th>  </th> <th> z </th> <th> mad </th> <th> tukey </th> <th> value </th>  </tr>
  <tr> <td> 36 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right"> 0.59 </td> </tr>
  <tr> <td> 37 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right"> 0.25 </td> </tr>
  <tr> <td> 38 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right"> -0.12 </td> </tr>
  <tr> <td> 39 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right"> 0.73 </td> </tr>
  <tr> <td> 40 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right"> 0.43 </td> </tr>
  <tr> <td> 41 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right"> -1.20 </td> </tr>
  <tr> <td> 42 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right"> -0.57 </td> </tr>
  <tr> <td> 43 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right"> -0.56 </td> </tr>
  <tr> <td> 44 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right"> 0.08 </td> </tr>
  <tr> <td> 45 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right"> -0.17 </td> </tr>
  <tr> <td> 46 </td> <td> TRUE </td> <td> TRUE </td> <td> TRUE </td> <td align="right">  </td> </tr>
  <tr> <td> 47 </td> <td> TRUE </td> <td> FALSE </td> <td> FALSE </td> <td align="right"> -16.43 </td> </tr>
  <tr> <td> 48 </td> <td> FALSE </td> <td> FALSE </td> <td> FALSE </td> <td align="right"> 79.00 </td> </tr>
  <tr> <td> 49 </td> <td> TRUE </td> <td> FALSE </td> <td> FALSE </td> <td align="right"> -20.59 </td> </tr>
  <tr> <td> 50 </td> <td> TRUE </td> <td> FALSE </td> <td> FALSE </td> <td align="right"> 18.30 </td> </tr>
   <a name=Not outliers with 3 methods, z, mad and tukey></a>
</table>


### Multivariate outliers




```r
outlier <- X %>% dplyr::select_if(is.numeric) %>% is_mult_outlier(method = "BEM")
## alpha should be less than 0.5:
##         alpha set to 1 - alpha
##  0.9995
multivariateOutlier <- X
multivariateOutlier$outlier <- outlier$outind
multivariateOutlier$dist <- outlier$dist
```
<!-- html table generated in R 4.2.2 by xtable 1.8-4 package -->
<!-- Thu Sep 14 10:32:19 2023 -->
<table border=1>
<caption align="bottom"> Multivariate outliers </caption>
<tr> <th>  </th> <th> a </th> <th> b </th> <th> c </th> <th> outlier </th> <th> dist </th>  </tr>
  <tr> <td> r1 </td> <td align="right"> 0.34 </td> <td align="right"> 3.47 </td> <td align="right"> 1.23 </td> <td> FALSE </td> <td align="right"> 2.96 </td> </tr>
  <tr> <td> r2 </td> <td align="right"> 1.37 </td> <td align="right"> 80.00 </td> <td align="right"> -0.75 </td> <td> TRUE </td> <td align="right"> 538.66 </td> </tr>
  <tr> <td> r3 </td> <td align="right"> -0.55 </td> <td align="right"> 0.19 </td> <td align="right"> 0.25 </td> <td> FALSE </td> <td align="right"> 0.69 </td> </tr>
  <tr> <td> r4 </td> <td align="right"> -1.57 </td> <td align="right"> -1.14 </td> <td align="right"> 0.56 </td> <td> FALSE </td> <td align="right"> 3.45 </td> </tr>
  <tr> <td> r5 </td> <td align="right">  </td> <td align="right"> 0.12 </td> <td align="right"> 0.79 </td> <td> FALSE </td> <td align="right"> 1.26 </td> </tr>
  <tr> <td> r6 </td> <td align="right"> -0.93 </td> <td align="right"> -1.75 </td> <td align="right"> -0.21 </td> <td> FALSE </td> <td align="right"> 1.79 </td> </tr>
  <tr> <td> r7 </td> <td align="right"> 20.00 </td> <td align="right"> 1.11 </td> <td align="right"> -0.17 </td> <td> TRUE </td> <td align="right"> 501.46 </td> </tr>
  <tr> <td> r8 </td> <td align="right"> -0.31 </td> <td align="right"> 2.22 </td> <td align="right"> -1.71 </td> <td> FALSE </td> <td align="right"> 4.37 </td> </tr>
  <tr> <td> r9 </td> <td align="right"> 0.65 </td> <td align="right"> 2.77 </td> <td align="right">  </td> <td> FALSE </td> <td align="right"> 1.06 </td> </tr>
  <tr> <td> r10 </td> <td align="right"> -1.34 </td> <td align="right"> -6.36 </td> <td align="right"> 1.65 </td> <td> FALSE </td> <td align="right"> 6.91 </td> </tr>
  <tr> <td> r11 </td> <td align="right"> 1.16 </td> <td align="right"> -5.32 </td> <td align="right"> -0.68 </td> <td> FALSE </td> <td align="right"> 3.88 </td> </tr>
  <tr> <td> r12 </td> <td align="right"> 1.67 </td> <td align="right"> -5.34 </td> <td align="right"> -1.30 </td> <td> FALSE </td> <td align="right"> 5.64 </td> </tr>
  <tr> <td> r13 </td> <td align="right"> 0.42 </td> <td align="right"> 2.34 </td> <td align="right"> -1.03 </td> <td> FALSE </td> <td align="right"> 1.17 </td> </tr>
  <tr> <td> r14 </td> <td align="right"> 1.41 </td> <td align="right"> 7.73 </td> <td align="right"> -0.84 </td> <td> FALSE </td> <td align="right"> 5.72 </td> </tr>
  <tr> <td> r15 </td> <td align="right"> 1.25 </td> <td align="right"> -0.44 </td> <td align="right"> -1.01 </td> <td> FALSE </td> <td align="right"> 1.35 </td> </tr>
  <tr> <td> r16 </td> <td align="right"> 0.10 </td> <td align="right"> -1.76 </td> <td align="right"> 0.00 </td> <td> FALSE </td> <td align="right"> 0.29 </td> </tr>
  <tr> <td> r17 </td> <td align="right"> 0.15 </td> <td align="right"> -2.31 </td> <td align="right"> -2.33 </td> <td> FALSE </td> <td align="right"> 5.26 </td> </tr>
  <tr> <td> r18 </td> <td align="right"> 0.18 </td> <td align="right"> 3.58 </td> <td align="right"> 0.73 </td> <td> FALSE </td> <td align="right"> 1.76 </td> </tr>
  <tr> <td> r19 </td> <td align="right"> 1.79 </td> <td align="right"> 0.15 </td> <td align="right"> 0.34 </td> <td> FALSE </td> <td align="right"> 4.12 </td> </tr>
  <tr> <td> r20 </td> <td align="right"> -0.09 </td> <td align="right"> 2.38 </td> <td align="right"> 0.66 </td> <td> FALSE </td> <td align="right"> 1.09 </td> </tr>
   <a name=Multivariate outlier detection and distance calculator></a>
</table>


### Multivariate outliers imputation

Now lets impute the missing and the multivariate outliers:



```r
outlier_imp <- is_mult_outlier_imp(X, outlier, method = "POEM")
## 
##  Number of completely missing observations  0 
## 
##  Number of complete and error-free observations:  16
miss <-(X %>% is.na %>% apply(1, any))
out <- outlier$outind
miss_or_outlier <- miss | out
imputed <- cbind(X[miss_or_outlier, ],
                 outlier_imp$imputed.data[miss_or_outlier, ])
names(imputed) <- paste0(rep(c("real_", "imputed_"), each = 3), names(imputed))
imputed$outlier <- outlier$outind[miss_or_outlier]
```
<!-- html table generated in R 4.2.2 by xtable 1.8-4 package -->
<!-- Thu Sep 14 10:32:19 2023 -->
<table border=1>
<caption align="bottom"> Multivariate outliers imputation </caption>
<tr> <th>  </th> <th> real_a </th> <th> real_b </th> <th> real_c </th> <th> imputed_a </th> <th> imputed_b </th> <th> imputed_c </th> <th> outlier </th>  </tr>
  <tr> <td> r2 </td> <td align="right"> 1.37 </td> <td align="right"> 80.00 </td> <td align="right"> -0.75 </td> <td align="right"> 1.41 </td> <td align="right"> 7.73 </td> <td align="right"> -0.84 </td> <td> TRUE </td> </tr>
  <tr> <td> r5 </td> <td align="right">  </td> <td align="right"> 0.12 </td> <td align="right"> 0.79 </td> <td align="right"> -1.57 </td> <td align="right"> 0.12 </td> <td align="right"> 0.79 </td> <td> FALSE </td> </tr>
  <tr> <td> r7 </td> <td align="right"> 20.00 </td> <td align="right"> 1.11 </td> <td align="right"> -0.17 </td> <td align="right"> 1.79 </td> <td align="right"> 0.15 </td> <td align="right"> 0.34 </td> <td> TRUE </td> </tr>
  <tr> <td> r9 </td> <td align="right"> 0.65 </td> <td align="right"> 2.77 </td> <td align="right">  </td> <td align="right"> 0.65 </td> <td align="right"> 2.77 </td> <td align="right"> -1.03 </td> <td> FALSE </td> </tr>
   <a name=Multivariate outlier imputation></a>
</table>


# Tools for markdown

List of functions in this category:

* `an9el_social` to include social media
* `badges.print`, to include selected badges in the spin file, (usually `README.R`)
* `readme.update`, to build the readme, and optionally update the man files

# Tools for document packages

From this [web](https://r-critique.com/a_few_tips_for_development_of_r_packages)

* `devtools::spell_check().` To check the spell of the hole package
* `usethis::use_gitlab_ci()` To create a gitlab-ci squeleton
* `codetools::checkUsagePackage`
* `googpractice::gp`, here the [web](https://github.com/MangoTheCat/goodpractice)
* `lintr::lint("normalizar.R")`. To check code style


# Coverage of the R package in gitlab-ci

To configure the coverage badge with ***gitlab-ci***, I follow the next steps:

1. Create a skeleton with `usethis::use_gitlab_ci()`. Modify it
as you wish.
1. Run the function `covr::gitlab()`, this create the `public`
folder. (Notice that the file `public/coverage.html` have the string
`Coverage: 50.03%`)
1. Create the link to coverage and pipeline badges in the `README.R` file. (See
this file as example)
1. Configure in [gitlab](https://gitlab.com/an9el/an9elutils) in
`"Settings" > "CI/CD" > "General Pipelines" > "Test coverage parsing"` the following
regular expression **`\sCoverage:\s*\d+.\d+%`**, which look for the string
`Coverage:`(with **`"\sCoverage:\s"`**) and for the percentage `50.03%` (with **`"\d+.\d+%"`**)


# SESSION INFO


<details closed>
<summary> <span title='Clik to Expand'> Current session info </span> </summary>

```r

─ Session info ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 setting  value
 version  R version 4.4.1 (2024-06-14)
 os       Ubuntu 24.04.1 LTS
 system   x86_64, linux-gnu
 ui       X11
 language (EN)
 collate  es_ES.UTF-8
 ctype    es_ES.UTF-8
 tz       Europe/Madrid
 date     2024-10-23
 pandoc   3.1.3 @ /usr/bin/ (via rmarkdown)

─ Packages ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 ! package       * version    date (UTC) lib source
 P an9elutils    * 0.7.0      2024-10-23 [?] gitlab (an9el/an9elutils@49bdc50)
   askpass         1.2.1      2024-10-04 [2] CRAN (R 4.4.1)
   backports       1.5.0      2024-05-23 [2] CRAN (R 4.4.0)
   base64enc       0.1-3      2015-07-28 [2] CRAN (R 4.3.1)
   bibtex          0.5.1      2023-01-26 [2] CRAN (R 4.3.1)
   brio            1.1.5      2024-04-24 [2] CRAN (R 4.3.1)
   cachem          1.1.0      2024-05-16 [2] CRAN (R 4.4.0)
   caret           6.0-94     2023-03-21 [2] CRAN (R 4.3.1)
   checkmate       2.3.2      2024-07-29 [2] CRAN (R 4.4.1)
   class           7.3-22     2023-05-03 [4] CRAN (R 4.4.0)
   cli             3.6.3      2024-06-21 [2] CRAN (R 4.4.1)
   clipr           0.8.0      2022-02-22 [2] CRAN (R 4.3.1)
   codetools       0.2-20     2024-03-31 [2] CRAN (R 4.3.1)
   collapse        2.0.16     2024-08-21 [2] CRAN (R 4.4.1)
   colorspace      2.1-1      2024-07-26 [2] CRAN (R 4.4.1)
   commonmark      1.9.1      2024-01-30 [2] CRAN (R 4.3.1)
   covr            3.6.4      2023-11-09 [2] CRAN (R 4.3.1)
   data.table      1.16.2     2024-10-10 [2] CRAN (R 4.4.1)
   DataExplorer  * 0.8.3      2024-01-24 [2] CRAN (R 4.3.1)
   datawizard      0.12.3     2024-09-02 [2] CRAN (R 4.4.1)
   desc            1.4.3      2023-12-10 [2] CRAN (R 4.3.1)
   details       * 0.3.0      2022-03-27 [2] CRAN (R 4.3.1)
   devtools        2.4.5      2022-10-11 [2] CRAN (R 4.3.1)
   digest          0.6.37     2024-08-19 [2] CRAN (R 4.4.1)
   dplyr         * 1.1.4      2023-11-17 [2] CRAN (R 4.3.1)
   ellipsis        0.3.2      2021-04-29 [2] CRAN (R 4.3.1)
   evaluate        1.0.1      2024-10-10 [2] CRAN (R 4.4.1)
   fansi           1.0.6      2023-12-08 [2] CRAN (R 4.3.1)
   fastmap         1.2.0      2024-05-15 [2] CRAN (R 4.4.0)
   forcats       * 1.0.0      2023-01-29 [2] CRAN (R 4.3.1)
   foreach         1.5.2      2022-02-02 [2] CRAN (R 4.3.1)
   fs              1.6.4      2024-04-25 [2] CRAN (R 4.3.1)
   future          1.34.0     2024-07-29 [2] CRAN (R 4.4.1)
   future.apply    1.11.2     2024-03-28 [2] CRAN (R 4.3.1)
   generics        0.1.3      2022-07-05 [2] CRAN (R 4.3.1)
   ggplot2       * 3.5.1      2024-04-23 [2] CRAN (R 4.3.1)
   globals         0.16.3     2024-03-08 [2] CRAN (R 4.3.1)
   glue          * 1.8.0      2024-09-30 [2] CRAN (R 4.4.1)
   gower           1.0.1      2022-12-22 [2] CRAN (R 4.3.1)
   gridExtra       2.3        2017-09-09 [2] CRAN (R 4.3.1)
   gtable          0.3.5      2024-04-22 [2] CRAN (R 4.3.1)
   hardhat         1.4.0      2024-06-02 [2] CRAN (R 4.4.0)
   here          * 1.0.1      2020-12-13 [2] CRAN (R 4.4.1)
   hms             1.1.3      2023-03-21 [2] CRAN (R 4.3.1)
   htmltools       0.5.8.1    2024-04-04 [2] CRAN (R 4.3.1)
   htmlwidgets     1.6.4      2023-12-06 [2] CRAN (R 4.3.1)
   httpuv          1.6.15     2024-03-26 [2] CRAN (R 4.3.1)
   httr            1.4.7      2023-08-15 [2] CRAN (R 4.3.1)
   igraph          2.1.1      2024-10-19 [2] CRAN (R 4.4.1)
   insight         0.20.4     2024-09-01 [2] CRAN (R 4.4.1)
   ipred           0.9-15     2024-07-18 [2] CRAN (R 4.4.1)
   iterators       1.0.14     2022-02-05 [2] CRAN (R 4.3.1)
   jsonlite        1.8.9      2024-09-20 [2] CRAN (R 4.4.1)
   kableExtra    * 1.4.0      2024-01-24 [2] CRAN (R 4.3.1)
   knitcitations * 1.0.12     2021-01-10 [2] CRAN (R 4.3.1)
   knitr         * 1.48       2024-07-07 [2] CRAN (R 4.4.1)
   later           1.3.2      2023-12-06 [2] CRAN (R 4.3.1)
   lattice         0.22-6     2024-03-20 [2] CRAN (R 4.3.1)
   lava            1.8.0      2024-03-05 [2] CRAN (R 4.3.1)
   lazyeval        0.2.2      2019-03-15 [2] CRAN (R 4.3.1)
   lifecycle       1.0.4      2023-11-07 [2] CRAN (R 4.3.1)
   listenv         0.9.1      2024-01-29 [2] CRAN (R 4.3.1)
   lubridate     * 1.9.3      2023-09-27 [2] CRAN (R 4.3.1)
   magick          2.8.5      2024-09-20 [2] CRAN (R 4.4.1)
   magrittr      * 2.0.3      2022-03-30 [2] CRAN (R 4.3.1)
   MASS            7.3-61     2024-06-13 [2] CRAN (R 4.4.0)
   Matrix          1.7-0      2024-04-26 [2] CRAN (R 4.4.0)
   matrixStats     1.4.1      2024-09-08 [2] CRAN (R 4.4.1)
   memoise         2.0.1      2021-11-26 [2] CRAN (R 4.3.1)
   mime            0.12       2021-09-28 [2] CRAN (R 4.3.1)
   miniUI          0.1.1.1    2018-05-18 [2] CRAN (R 4.3.1)
   ModelMetrics    1.2.2.2    2020-03-17 [2] CRAN (R 4.3.1)
   modi            0.1.2      2023-03-14 [2] CRAN (R 4.3.1)
   munsell         0.5.1      2024-04-01 [2] CRAN (R 4.3.1)
   networkD3       0.4        2017-03-18 [2] CRAN (R 4.3.1)
   nlme            3.1-166    2024-08-14 [4] CRAN (R 4.4.1)
   nnet            7.3-19     2023-05-03 [4] CRAN (R 4.3.3)
   openssl         2.2.2      2024-09-20 [2] CRAN (R 4.4.1)
   pacman        * 0.5.1      2019-03-11 [2] CRAN (R 4.3.1)
   pander          0.6.5      2022-03-18 [2] CRAN (R 4.3.1)
   parallelly      1.38.0     2024-07-27 [2] CRAN (R 4.4.1)
   pillar          1.9.0      2023-03-22 [2] CRAN (R 4.3.1)
   pkgbuild        1.4.4      2024-03-17 [2] CRAN (R 4.3.1)
   pkgconfig       2.0.3      2019-09-22 [2] CRAN (R 4.3.1)
   pkgload         1.4.0      2024-06-28 [2] CRAN (R 4.4.1)
   plyr          * 1.8.9      2023-10-02 [2] CRAN (R 4.3.1)
   png             0.1-8      2022-11-29 [2] CRAN (R 4.3.1)
   ppsr          * 0.0.5      2024-02-18 [2] CRAN (R 4.3.1)
   pROC            1.18.5     2023-11-01 [2] CRAN (R 4.3.1)
   prodlim         2024.06.25 2024-06-24 [2] CRAN (R 4.4.1)
   profvis         0.4.0      2024-09-20 [2] CRAN (R 4.4.1)
   promises        1.3.0      2024-04-05 [2] CRAN (R 4.3.1)
   pryr            0.1.6      2023-01-17 [2] CRAN (R 4.3.1)
   purrr         * 1.0.2      2023-08-10 [2] CRAN (R 4.3.1)
   R6              2.5.1      2021-08-19 [2] CRAN (R 4.3.1)
   rapportools     1.1        2022-03-22 [2] CRAN (R 4.3.1)
   Rcpp            1.0.13     2024-07-17 [2] CRAN (R 4.4.1)
   readr         * 2.1.5      2024-01-10 [2] CRAN (R 4.3.1)
   recipes         1.1.0      2024-07-04 [2] CRAN (R 4.4.1)
   RefManageR      1.4.0      2022-09-30 [2] CRAN (R 4.3.1)
   remotes         2.5.0      2024-03-17 [2] CRAN (R 4.3.1)
   reshape2        1.4.4      2020-04-09 [2] CRAN (R 4.3.1)
   rex             1.2.1      2021-11-26 [2] CRAN (R 4.3.1)
   rlang           1.1.4      2024-06-04 [2] CRAN (R 4.4.0)
   rmarkdown     * 2.28       2024-08-17 [2] CRAN (R 4.4.1)
   roxygen2        7.3.2      2024-06-28 [2] CRAN (R 4.4.1)
   rpart           4.1.23     2023-12-05 [4] CRAN (R 4.4.0)
   rprojroot       2.0.4      2023-11-05 [2] CRAN (R 4.3.1)
   rstudioapi      0.16.0     2024-03-24 [2] CRAN (R 4.3.1)
   scales          1.3.0      2023-11-28 [2] CRAN (R 4.3.1)
   sessioninfo     1.2.2      2021-12-06 [2] CRAN (R 4.3.1)
   shiny           1.9.1      2024-08-01 [2] CRAN (R 4.4.1)
   stringi         1.8.4      2024-05-06 [2] CRAN (R 4.3.3)
   stringr       * 1.5.1      2023-11-14 [2] CRAN (R 4.3.1)
   summarytools  * 1.0.1      2022-05-20 [2] CRAN (R 4.3.1)
   survival        3.7-0      2024-06-05 [4] CRAN (R 4.4.0)
   svglite         2.1.3      2023-12-08 [2] CRAN (R 4.3.1)
   systemfonts     1.1.0      2024-05-15 [2] CRAN (R 4.4.0)
   testthat        3.2.1.1    2024-04-14 [2] CRAN (R 4.3.1)
   tibble        * 3.2.1      2023-03-20 [2] CRAN (R 4.3.1)
   tidyr         * 1.3.1.9000 2024-08-29 [2] Github (tidyverse/tidyr@622afe8)
   tidyselect      1.2.1      2024-03-11 [2] CRAN (R 4.3.1)
   tidyverse     * 2.0.0      2023-02-22 [2] CRAN (R 4.3.1)
   timechange      0.3.0      2024-01-18 [2] CRAN (R 4.3.1)
   timeDate        4041.110   2024-09-22 [2] CRAN (R 4.4.1)
   tzdb            0.4.0      2023-05-12 [2] CRAN (R 4.3.1)
   urlchecker      1.0.1      2021-11-30 [2] CRAN (R 4.3.1)
   usethis         3.0.0      2024-07-29 [2] CRAN (R 4.4.1)
   utf8            1.2.4      2023-10-22 [2] CRAN (R 4.3.1)
   vctrs           0.6.5      2023-12-01 [2] CRAN (R 4.3.1)
   viridis         0.6.5      2024-01-29 [2] CRAN (R 4.3.1)
   viridisLite     0.4.2      2023-05-02 [2] CRAN (R 4.3.1)
   visNetwork    * 2.1.2      2022-09-29 [2] CRAN (R 4.3.1)
   withr           3.0.1      2024-07-31 [2] CRAN (R 4.4.1)
   xfun            0.48       2024-10-03 [2] CRAN (R 4.4.1)
   xml2            1.3.6      2023-12-04 [2] CRAN (R 4.3.1)
   xtable          1.8-4      2019-04-21 [2] CRAN (R 4.3.1)
   yaml            2.3.10     2024-07-26 [2] CRAN (R 4.4.1)

 [1] /home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4
 [2] /usr/local/lib/R/site-library
 [3] /usr/lib/R/site-library
 [4] /usr/lib/R/library

 P ── Loaded and on-disk path mismatch.

─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

```

</details>
<br>

