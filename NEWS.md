# an9elutils 0.7.1

# an9elutils 0.7.0

* pass `recode_sex` to the package an9elpedigree
* Added function to encrypt character vectors.

# an9elutils 0.6.0

# an9elutils 0.5.0

* Added function to recode sex to numeric, Male = 0, Female = 1
* Added 'library' function, to load multiple packages without quotation marks
* Added function "p" to paste without quotation marks

# an9elutils 0.4.0

* Use of codecov

# an9elutils 0.3.0

* Added function \code{percent_value}
* Added function \code{plot_importance}
* Added function \code{plot_confusion}
* Added examples to the README file
* Added function \code{factor_labels}
* Change behaviour of \code{to_factor} now can change the labels of the factors
* Start using collapse to increase speed

# an9elutils 0.2.0

* Remove annotation functions to the package an9elannot
* Add getobj package
* Add gitlab-ci
* Added \code{to_factor} function to code as factor columns
* Add \code{pps.rm.feature} function to remove one feature
  from the pps results
* Added function to remove near Zero variance features
* Added function to summarise a data.frame
* Added function \code{isnt_out} to create functions to detect univariate NO outliers
* Added function \code{is_mult_outlier} for multivariate outliers detection
* Added function \code{is_mult_outlier_imp} for multivariate outliers imputation

# an9elutils 0.1.0

* Added function to remove interactions between nodes of a
category from the pps results
* Added function to remove one feature from the pps results

# an9elutils 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
* Pass goodpractice::gp()
