test_that("drop nearZero", {
    df <- mtcars %>% dplyr::arrange(gear, carb, cyl) %>% head(16)
    df_nzYES <- drop_nearZero(df, freqCut = 90/10, uniqueCut = 12.5)
    checkmate::expect_data_frame(df_nzYES, nrows = 16, ncols = 9)
    df_nzNO <- drop_nearZero(df, freqCut = 90/10, uniqueCut = 12.4)
    checkmate::expect_data_frame(df_nzNO, nrows = 16, ncols = 11)
})
