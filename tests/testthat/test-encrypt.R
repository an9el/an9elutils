testthat::test_that(
  desc = "encrypt test", {
    values <- c("sensitive data 1", "sensitive data 2", NA, "sensitive data 3")
    key <- "my_secret_key"

    # Encriptar los valores
    encrypted_values <- encrypt_vector(values, key)

    # Verificar que la longitud sea la misma
    expect_length(encrypted_values, length(values))

    # Verificar que ningún valor encriptado sea igual a los originales
    expect_false(any(encrypted_values == values, na.rm = TRUE))

    # Desencriptar los valores encriptados
    decrypted_values <- encrypt_vector(encrypted_values, key, decrypt = TRUE)

    # Verificar que los valores desencriptados coincidan con los originales
    expect_equal(decrypted_values, values)

    # Verificar que los valores NA sigan siendo NA después de la desencriptación
    expect_true(is.na(decrypted_values[3]))
  })

